﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using RestSharp.Authenticators;
using Sitrad.Api.Client.Models;
using Sitrad.Api.V1;
using Sitrad.Api.V1.Bodies;

namespace Sitrad.Api.Client
{
    public class V1Client
    {
        private readonly RestClient _apiClient;

        public V1Client(ServerConfiguration serverConfiguration)
        {
            _apiClient = new RestClient(serverConfiguration.SitradApiUrl)
            {
                Authenticator = new HttpBasicAuthenticator(serverConfiguration.User, serverConfiguration.Password),

            };

            if (serverConfiguration.IgnoreCertificateValidation)
                _apiClient.RemoteCertificateValidationCallback += (sender, certificate, chain, sslPolicyErrors) => true;
        }

        public Instrument GetInstrument(int instrumentId, out Request requestInfo)
        {
            var resource = $"instruments/{instrumentId}";
            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObject<Instrument>>(response.Content).Result
                : null;
        }

        public List<Alarm> GetInstrumentAlarms(int instrumentId, DateTime startDate, DateTime endDate, int? alarmStatus, out Request requestInfo)
        {
            var startDateIso = startDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
            var endDateISo = endDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");

            var resource = $"instruments/{instrumentId}/alarms?startDate={startDateIso}&endDate={endDateISo}";

            if (alarmStatus != null)
                resource += $"&alarmStatus={(AlarmStatusEnum)alarmStatus}";

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObjects<Alarm>>(response.Content).Result
                : null;
        }

        public List<InstrumentProperty> GetInstrumentValues(int instrumentId, DateTime? startDate, DateTime? endDate, List<string> codes, out Request requestInfo)
        {
            var resource = $"instruments/{instrumentId}/values";
            var queryParameters = string.Empty;

            if (startDate != null && endDate != null)
            {
                var startDateIso = startDate.Value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
                var endDateISo = endDate.Value.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
                queryParameters += $"&startDate={startDate}&endDate{endDate}";
            }

            if (codes?.Count > 0)
            {
                var codesString = string.Join(',', codes).Trim();
                queryParameters += $"&codes={codesString}";
            }

            if (queryParameters.Length > 0)
            {
                queryParameters = queryParameters.Remove(0, 1);
                queryParameters = queryParameters.Insert(0, "?");
            }

            resource += queryParameters;

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObjects<InstrumentProperty>>(response.Content).Result
                : null;
        }

        public List<InstrumentFunction> GetInstrumentFunctions(int instrumentId, int? groupCode, out Request requestInfo)
        {
            var resource = $"instruments/{instrumentId}/functions";
            var queryParameters = string.Empty;

            if (groupCode != null)
            {
                queryParameters += $"&groupCode={groupCode}";
            }

            if (queryParameters.Length > 0)
            {
                queryParameters = queryParameters.Remove(0, 1);
                queryParameters = queryParameters.Insert(0, "?");
            }

            resource += queryParameters;

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObjects<InstrumentFunction>>(response.Content).Result
                : null;
        }

        public InstrumentFunction GetInstrumentFunction(int instrumentId, string code, int? groupCode, out Request requestInfo)
        {
            var resource = $"instruments/{instrumentId}/functions/{code.Trim()}";
            var queryParameters = string.Empty;

            if (groupCode != null)
            {
                queryParameters += $"&groupCode={groupCode}";
            }

            if (queryParameters.Length > 0)
            {
                queryParameters = queryParameters.Remove(0, 1);
                queryParameters = queryParameters.Insert(0, "?");
            }

            resource += queryParameters;

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObject<InstrumentFunction>>(response.Content).Result
                : null;
        }
        
        public List<Instrument> GetInstruments(int? instrumentStatus, out Request requestInfo)
        {
            var resource = $"instruments";

            if (instrumentStatus != null)
                resource += $"?instrumentStatus={(InstrumentStatusEnum)instrumentStatus}";

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObjects<Instrument>>(response.Content).Result
                : new List<Instrument>();
        }

        public List<Alarm> GetAlarms(DateTime startDate, DateTime endDate, int? alarmStatus, out Request requestInfo)
        {
            var startDateIso = startDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");
            var endDateISo = endDate.ToUniversalTime().ToString("yyyy-MM-ddTHH:mm:ssZ");

            var resource = $"alarms?startDate={startDateIso}&endDate={endDateISo}";

            if (alarmStatus != null)
                resource += $"&alarmStatus={(AlarmStatusEnum)alarmStatus}";

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObjects<Alarm>>(response.Content).Result
                : null;
        }

        public List<Converter> GetConverters(int? converterStatus, out Request requestInfo)
        {
            var resource = $"converters";

            if (converterStatus != null)
                resource += $"?converterStatus={(ConverterStatusEnum)converterStatus}";

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObjects<Converter>>(response.Content).Result
                : null;

        }

        public List<Instrument> GetConverterInstruments(int converterId, int? instrumentStatus, out Request requestInfo)
        {
            var resource = $"converters/{converterId}/instruments";

            if (instrumentStatus != null)
                resource += $"?instrumentStatus={(InstrumentStatusEnum)instrumentStatus}";

            var request = new RestRequest(resource, Method.GET);
            request.AddHeader("accept-language", "en");

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo)
                ? JsonConvert.DeserializeObject<ResponseObjects<Instrument>>(response.Content).Result
                : new List<Instrument>();
        }

        public bool UpdateInstrument(int instrumentId, PutInstrument putInstrument, out Request requestInfo)
        {

            var resource = $"instruments/{instrumentId}";
            var JsonBody = JsonConvert.SerializeObject(putInstrument);


            var request = new RestRequest(resource, Method.PUT, DataFormat.Json);
            request.AddHeader("accept-language", "en");
            request.AddParameter("application/json", JsonBody, ParameterType.RequestBody);

            var response = _apiClient.Execute(request);

            return IsValidResponse(response, out requestInfo);
        }
        
        private static bool IsValidResponse(IRestResponse response, out Request requestInfo)
        {
            requestInfo = new Request
            {
                StatusCode = response.StatusCode,
                Body = response.Content
            };

            if (!string.IsNullOrEmpty(response.ErrorMessage))
            {
                requestInfo.Errors.Add(response.ErrorMessage);
                return false;
            }

            try
            {
                JObject.Parse(response.Content);

                if (response.StatusCode != HttpStatusCode.OK)
                    requestInfo.Errors = JsonConvert.DeserializeObject<ResponseErrors>(response.Content).Errors.ToList();
            }
            catch
            {
                requestInfo.Errors.Add("Body empty or not in JSON format - Using HTTP Status Code to verify");
                return false;
            }

            return response.StatusCode == HttpStatusCode.OK;
        }
    }
}
