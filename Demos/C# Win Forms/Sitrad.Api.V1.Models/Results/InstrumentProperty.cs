﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class InstrumentProperty : IResult
    {
        /// <summary>
        /// Unique identifier of property
        /// </summary>
        [JsonProperty("code")]
        public string Code { get; set; }
        
        [JsonProperty("name")]
        public string Name { get; set; }
        
        [JsonProperty("values")]
        public InstrumentPropertyValue[] InstrumentPropertyValues { get; set; }
    }
}
