﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class AlarmInhibitionSchedule : IResult
    {
        /// <summary>
        /// Unique identifier of schedule
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }
        
        [JsonProperty("subject")] 
        public string Subject { get; set; }

        [JsonProperty("typeId")]
        public ScheduleTypeEnum TypeId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("startDate")]
        public string StartDate { get; set; }

        [JsonProperty("endDate")]
        public string EndDate { get; set; }

        [JsonProperty("isDaily")]
        public bool? AllDay { get; set; }

        [JsonProperty("recurrenceRule")]
        public string RecurrenceRule { get; set; }
        
        [JsonProperty("startRecurrenceDate")]
        public string StartRecurrenceDate { get; set; }
        
        [JsonProperty("endRecurrenceDate")]
        public string EndRecurrenceDate { get; set; }
        
        [JsonProperty("limitRecurrence")]
        public int? LimitRecurrence { get; set; }
        
        [JsonProperty("creationDate")]
        public string CreateDate { get; set; }
    }
}
