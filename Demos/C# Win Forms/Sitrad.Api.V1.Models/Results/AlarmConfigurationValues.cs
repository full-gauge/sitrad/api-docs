﻿using System.Runtime.Serialization;
using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class AlarmConfigurationValues : IResult
    {
        /// <summary>
        /// Function or Property Name from instrument value
        /// </summary>
        [JsonProperty("attributeName")]
        public string AttributeName { get; set; }

        /// <summary>
        /// It is the function description defined in the resource file. It can be empty if developer doesn't fulfill it.
        /// </summary>
        [JsonProperty("description")]
        public string Description { get; set; }

        /// <summary>
        /// Property that identifies the property order.
        /// </summary>
        [JsonProperty("reportIndex")]
        public int? ReportIndex { get; set; }

        /// <summary>
        /// Property Name.
        /// It is identical to C# class property name.
        /// </summary>
        [JsonProperty("propertyName")]
        public string PropertyName { get; set; }

        /// <summary>
        /// Property value. It can be empty if developer doesn't fulfill it.
        /// </summary>
        [JsonProperty("value")]
        public object Value { get; set; }

        /// <summary>
        /// Property valueType.
        /// </summary>
        [JsonProperty("valueType")]
        public string ValueType { get; set; }

        /// <summary>
        /// Property that indicates if property is in error.
        /// </summary>
        [JsonProperty("isInError")]
        public bool IsInError { get; set; }

        /// <summary>
        /// Property that indicates if property is enabled.
        /// </summary>
        [JsonProperty("isEnable")]
        public bool IsEnabled { get; set; }


        /// <summary>
        /// Property that indicates how many decimal places has the value number.
        /// </summary>
        [JsonProperty("decimalPlaces")]
        public int DecimalPlaces { get; set; }


        /// <summary>
        /// Gets or sets the MeasurementUnityEnum
        /// </summary>
        [DataMember(Name = "measurementUnityId", EmitDefaultValue = false)]
        [JsonProperty("measurementUnityId")]
        public MeasurementUnityEnum MeasurementUnityId { get; set; }

        /// <summary>
        /// Get measurement unity as string.
        /// </summary>
        [JsonProperty("measurementUnity")]
        public string MeasurementUnity { get; set; }
    }
}