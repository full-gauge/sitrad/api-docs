﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class InstrumentPropertyValue : IResult
    {
        [JsonProperty("date")]
        public string Date { get; set; }

        [JsonProperty("value")]
        public object Value { get; set; }

        [JsonProperty("decimalPlaces")]
        public int DecimalPlaces { get; set; }

        [JsonProperty("isFailPayload")]
        public bool IsFailPayload { get; set; }

        [JsonProperty("measurementUnityId")]
        public MeasurementUnityEnum MeasurementUnityId { get; set; }

        [JsonProperty("measurementUnity")]
        public string MeasurementUnity { get; set; }
    }
}
