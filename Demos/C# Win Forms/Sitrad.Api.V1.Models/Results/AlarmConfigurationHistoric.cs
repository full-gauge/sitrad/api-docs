﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class AlarmConfigurationHistoric : IResult
    {
        [JsonProperty("instrumentId")] 
        public int InstrumentId { get; set; }

        [JsonProperty("userId")] 
        public int? UserId { get; set; }

        [JsonProperty("alarmCode")]
        public string AlarmCode { get; set; }

        [JsonProperty("maxValue")] 
        public double MaxValue { get; set; }

        [JsonProperty("minValue")] 
        public double MinValue { get; set; }

        [JsonProperty("ignoreReport")] 
        public bool IgnoreReport { get; set; }

        [JsonProperty("instrumentProperty")]
        public string InstrumentProperty { get; set; }

        [JsonProperty("delayTime")]
        public int DelayTime { get; set; }

        [JsonProperty("startDate")] 
        public string StartDate { get; set; }

        [JsonProperty("endDate")] 
        public string EndDate { get; set; }
    }
}