﻿namespace Sitrad.Api.V1
{
    
    public enum ScheduleTypeEnum
    {
        /// <summary>
        /// None
        /// </summary>
        undefined = 0,

        /// <summary>
        /// Schedule alarm inhibition during defrost
        /// </summary>
        alarmInhibitionDuringDefrost = 1,

        /// <summary>
        /// Defrost schedule(directly on the instrument)
        /// </summary>
        defrost = 2,

        /// <summary>
        /// Schedule events(directly on the instrument)
        /// </summary>
        internalEvent = 3,

        /// <summary>
        /// Manual alarm schedule
        /// </summary>
        manualAlarmInhibition = 4,

        /// <summary>
        /// Macro execution schedule
        /// </summary>
        macroScheduling = 5,

        /// <summary>
        /// Report submission schedule
        /// </summary>
        reportScheduling = 6,

        /// <summary>
        /// Scheduled alarm inhibition schedule
        /// </summary>
        alarmInhibitionScheduling = 7,

        /// <summary>
        /// Delay alarm inhibition schedule
        /// </summary>
        alarmInhibitionByDelay = 8,

        /// <summary>
        /// Backup periodicity setting (NOT integrated into schedules)
        /// </summary>
        backup = 9
    }
}
