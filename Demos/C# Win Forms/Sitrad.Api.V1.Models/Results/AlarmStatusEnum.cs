﻿namespace Sitrad.Api.V1
{
    public enum AlarmStatusEnum
    {
        started = 1,
        finalized = 2,
        unfinalized = 3,
        reconized = 4,
        unreconized = 5,
        inhibited = 6,
        uninhibited = 7,
    }
}
