﻿using System.ComponentModel;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class Macro : IResult
    {
        /// <summary>
        /// Unique identifier of macro
        /// </summary>
        [Description("id")]
        public int Id { get; set; }

        /// <summary>
        /// Unique identifier of user
        /// </summary>
        [Description("userId")]
        public int UserId { get; set; }

        /// <summary>
        /// Macro name
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Macro creation date
        /// </summary>
        [Description("creationDate")]
        public string CreationDate { get; set; }
    }
}
