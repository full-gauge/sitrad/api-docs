﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    public class Converter : IResult
    {
        /// <summary>
        /// Unique identifier of converter
        /// </summary>
        [JsonProperty("id")]
        public int Id { get; set; }

        [JsonProperty("name")]
        public string Name { get; set; }

        [JsonProperty("statusId")]
        public ConverterStatusEnum StatusId { get; set; }
        
        [JsonProperty("status")]
        public string Status { get; set; }

        [JsonProperty("statusDescription")]
        public string StatusDescription { get; set; }

        [JsonProperty("typeId")]
        public ConverterTypeEnum TypeId { get; set; }

        [JsonProperty("type")]
        public string Type { get; set; }

        [JsonProperty("version")]
        public string Version { get; set; }

        /// <summary>
        /// in milliseconds
        /// </summary>
        [JsonProperty("communicationTimeout")]
        public int CommunicationTimeout { get; set; }

        /// <summary>
        /// in seconds
        /// </summary>
        [JsonProperty("savePayloadInterval")]
        public int SavePayloadInterval { get; set; }

        /// <summary>
        /// in seconds
        /// </summary>
        [JsonProperty("communicationFailInterval")]
        public int CommunicationFailInterval { get; set; }
    }
}
