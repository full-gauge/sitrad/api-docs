﻿using System.Runtime.Serialization;
using Newtonsoft.Json;

namespace Sitrad.Api.V1
{
    /// <summary>
    /// Response base for all responses
    /// </summary>
    public class BaseResponse
    {
        /// <summary>
        /// HTTP Status code
        /// </summary>
        [JsonProperty("status")]
        public HttpStatusCodeEnum StatusCode { get; set; }
    }

    /// <summary>
    /// HTTP status codes
    /// </summary>
    public enum HttpStatusCodeEnum
    {
        [EnumMember] Ok = 200,
        [EnumMember] Accepted = 202,
        [EnumMember] BadRequest = 400,
        [EnumMember] Unauthorized = 401,
        [EnumMember] InternalServerError = 500
    }
}
