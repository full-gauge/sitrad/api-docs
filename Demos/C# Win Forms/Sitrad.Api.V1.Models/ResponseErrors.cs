﻿using System;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace Sitrad.Api.V1
{
    public class ResponseErrors : BaseResponse
    {
        public ResponseErrors()
        {
            
        }

        public ResponseErrors(HttpStatusCodeEnum httpStatusCode, Guid errorIdentifier, string error)
            : this(httpStatusCode, errorIdentifier, new List<string> {error})
        {
            
        }

        public ResponseErrors(HttpStatusCodeEnum httpStatusCode, Guid errorIdentifier, List<string> errorsList)
        {
            Identifier = errorIdentifier;
            StatusCode = httpStatusCode;
            Errors = errorsList.ToArray();
        }

        /// <summary>
        /// identifier of errors information
        /// </summary>
        [JsonProperty("identifier")]
        public Guid Identifier { get;  set; }

        /// <summary>
        /// List of errors descriptions
        /// </summary>
        [JsonProperty("errors")]
        public string[] Errors { get; set; }
    }
}
