﻿using System.ComponentModel;
using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    /// <summary>
    /// Object to create a instrument alarm inhibition
    /// </summary>
    [Description("")]
    public class PostInstrumentAlarmInhibition : IBody
    {
        [JsonProperty("scheduleType")]
        public ScheduleTypeEnum ScheduleTypeEnum { get; set; }

        /// <summary>
        /// End date for the inhibition in ISO8601 UTC format
        /// </summary>
        [JsonProperty("endDate")]
        public string EndDate { get; set; }
    }
}
