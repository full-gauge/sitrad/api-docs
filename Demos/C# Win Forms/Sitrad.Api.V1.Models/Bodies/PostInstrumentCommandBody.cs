﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    /// <summary>
    /// Object to create a instrument command
    /// </summary>
    public class PostInstrumentCommand : IBody
    {
        [JsonProperty("code")]
        public string Code { get; set; }

        [JsonProperty("value")]
        public double? Value { get; set; }
        
        [JsonProperty("groupCode")]
        public int? GroupCode { get; set; }

        [JsonProperty("showSpc")]
        public bool? ShowSpc { get; set; }
    }
}
