﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1
{
    /// <summary>
    /// Object to create a macro execution
    /// </summary>
    public class PostMacroExecution : IBody
    {
        [JsonProperty("id")]
        public string Id { get; set; }
    }
}
