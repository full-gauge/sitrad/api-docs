﻿using Newtonsoft.Json;
using Sitrad.Api.V1.Interfaces;

namespace Sitrad.Api.V1.Bodies
{
    /// <summary>
    /// Object to create a preset execution
    /// </summary>
    public class PostPresetExecution : IBody
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("instrumentId")]
        public string InstrumentId { get; set; }
    }
}
