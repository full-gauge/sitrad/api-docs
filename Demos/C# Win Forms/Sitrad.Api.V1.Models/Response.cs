﻿using System.Net;

namespace Sitrad.Api.V1
{
    /// <summary>
    /// Response body for cases where only the status needs to be sent.
    /// </summary>
    public class Response : BaseResponse
    {
        public Response()
        {
            StatusCode = (HttpStatusCodeEnum)HttpStatusCode.OK;
        }
    }
}
