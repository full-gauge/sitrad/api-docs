﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows.Forms;
using Sitrad.Api.Client;
using Sitrad.Api.V1.Presentation.Forms.Models;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormInstruments : FormBase
    {
        private List<Instrument> _instruments = new List<Instrument>();

        public FormInstruments(V1Client apiV1Client, Converter converter = null)
            : base(apiV1Client)
        {
            InitializeComponent();

            txtConverterId.Text = converter != null
                ? converter.Id.ToString()
                : string.Empty;

            var items = Enum.GetValues(typeof(InstrumentStatusEnum))
                .Cast<InstrumentStatusEnum>()
                .Select(x => new ComboBoxItem((int)x, x.ToString()))
                .ToList();

            items.Insert(0, new ComboBoxItem(-1, string.Empty));

            cmbInstrumentStatus.DisplayMember = "DisplayValue";
            cmbInstrumentStatus.DataSource = items;

            // Initialize the DataGridView.
            dgvInstruments.AutoGenerateColumns = false;
            dgvInstruments.AutoSize = true;
            dgvInstruments.MultiSelect = false;
            dgvInstruments.SelectionMode = DataGridViewSelectionMode.FullRowSelect;

            foreach (var property in typeof(Instrument).GetProperties())
            {
                // Initialize and add a text box column.
                DataGridViewColumn column = new DataGridViewTextBoxColumn();
                column.DataPropertyName = property.Name;
                column.Name = property.Name;
                dgvInstruments.Columns.Add(column);
            }

            dgvInstruments.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.Fill;

            FillInstruments();
        }

        private void FillInstruments()
        {
            var converterId = txtConverterId.Text;
            var instrumentStatus = ((ComboBoxItem)cmbInstrumentStatus.SelectedItem).Value >= 0
                ? ((ComboBoxItem)cmbInstrumentStatus.SelectedItem).Value
                : (int?)null;

            _instruments = string.IsNullOrEmpty(converterId)
            ? _apiV1Client.GetInstruments(instrumentStatus, out _requestInfo)
            : _apiV1Client.GetConverterInstruments(int.Parse(converterId), instrumentStatus, out _requestInfo);

            dgvInstruments.DataSource = _instruments;
            dgvInstruments.Refresh();

            btnGoToInstrument.Enabled = _instruments.Any();

            if (_requestInfo.Errors.Any())
                ShowResponse();
        }

        private void dgvInstruments_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            if (e.RowIndex < 0)
                return;

            var instrument = (Instrument)dgvInstruments.Rows[e.RowIndex].DataBoundItem;

            OpenFormInstrument(instrument);
        }

        private void btnResponseInfos_Click(object sender, System.EventArgs e)
        {
            ShowResponse();
        }

        private void lblGoToInstrument_Click(object sender, System.EventArgs e)
        {
            var rowSelected = dgvInstruments.Rows.GetFirstRow(DataGridViewElementStates.Selected);
            var instrument = (Instrument)dgvInstruments.Rows[rowSelected].DataBoundItem;

            OpenFormInstrument(instrument);
        }

        private void OpenFormInstrument(Instrument instrument)
        {
            var result = _apiV1Client.GetInstrument(instrument.Id, out _requestInfo);

            if (_requestInfo.Errors.Any())
            {
                ShowResponse();
                return;
            }

            var formInstruments = new FormInstrument(_apiV1Client, result);
            formInstruments.ShowDialog();
            formInstruments.Dispose();

            FillInstruments();
        }

        private void btnGetInstruments_Click(object sender, EventArgs e)
        {
            FillInstruments();
        }
    }
}
