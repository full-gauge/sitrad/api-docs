﻿using System;
using System.Linq;
using System.Windows.Forms;
using Sitrad.Api.Client;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormFunction : FormBase
    {
        private Instrument _instrument;
        private InstrumentFunction _instrumentFunction;
        
        public FormFunction(V1Client apiV1Client, Instrument instrument, InstrumentFunction instrumentFunction)
        {
            InitializeComponent();

            StartPosition = FormStartPosition.CenterParent;
            base.Text = $"Funtion";

            _apiV1Client = apiV1Client;
            _instrument = instrument;
            _instrumentFunction = instrumentFunction;

            txtId.Text = instrument.Id.ToString();
            txtName.Text = instrument.Name;
        }

        private void FillValues()
        {
            var instrumentId = int.Parse(txtId.Text);
            var functionId = txtName.Text.Trim();
            
            _instrumentFunction = _apiV1Client.GetInstrumentFunction(instrumentId, functionId, null, out _requestInfo);

            if (_requestInfo.Errors.Any())
            {
                ShowResponse();
                return;
            }
        }
        
        private void btnGetValues_Click(object sender, EventArgs e)
        {
           FillValues();
        }
    }
}
