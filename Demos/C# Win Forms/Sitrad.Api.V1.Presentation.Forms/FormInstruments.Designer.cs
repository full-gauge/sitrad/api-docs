﻿
namespace Sitrad.Api.V1.Presentation.Forms
{
    partial class FormInstruments
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvInstruments = new System.Windows.Forms.DataGridView();
            this.btnResponseInfos = new System.Windows.Forms.Button();
            this.btnGoToInstrument = new System.Windows.Forms.Button();
            this.txtConverterId = new System.Windows.Forms.TextBox();
            this.lblConverterId = new System.Windows.Forms.Label();
            this.lblInstrumentStatus = new System.Windows.Forms.Label();
            this.cmbInstrumentStatus = new System.Windows.Forms.ComboBox();
            this.btnGetInstruments = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstruments)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvInstruments
            // 
            this.dgvInstruments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvInstruments.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvInstruments.Location = new System.Drawing.Point(12, 52);
            this.dgvInstruments.Name = "dgvInstruments";
            this.dgvInstruments.RowTemplate.Height = 25;
            this.dgvInstruments.Size = new System.Drawing.Size(1002, 491);
            this.dgvInstruments.TabIndex = 0;
            this.dgvInstruments.CellDoubleClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvInstruments_CellDoubleClick);
            // 
            // btnResponseInfos
            // 
            this.btnResponseInfos.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.btnResponseInfos.Location = new System.Drawing.Point(12, 555);
            this.btnResponseInfos.Name = "btnResponseInfos";
            this.btnResponseInfos.Size = new System.Drawing.Size(107, 23);
            this.btnResponseInfos.TabIndex = 1;
            this.btnResponseInfos.Text = "Response Info";
            this.btnResponseInfos.UseVisualStyleBackColor = true;
            this.btnResponseInfos.Click += new System.EventHandler(this.btnResponseInfos_Click);
            // 
            // btnGoToInstrument
            // 
            this.btnGoToInstrument.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGoToInstrument.Enabled = false;
            this.btnGoToInstrument.Location = new System.Drawing.Point(846, 555);
            this.btnGoToInstrument.Name = "btnGoToInstrument";
            this.btnGoToInstrument.Size = new System.Drawing.Size(168, 23);
            this.btnGoToInstrument.TabIndex = 2;
            this.btnGoToInstrument.Text = "Instrument details...";
            this.btnGoToInstrument.UseVisualStyleBackColor = true;
            this.btnGoToInstrument.Click += new System.EventHandler(this.lblGoToInstrument_Click);
            // 
            // txtConverterId
            // 
            this.txtConverterId.Location = new System.Drawing.Point(90, 13);
            this.txtConverterId.Name = "txtConverterId";
            this.txtConverterId.Size = new System.Drawing.Size(225, 23);
            this.txtConverterId.TabIndex = 3;
            // 
            // lblConverterId
            // 
            this.lblConverterId.AutoSize = true;
            this.lblConverterId.Location = new System.Drawing.Point(12, 16);
            this.lblConverterId.Name = "lblConverterId";
            this.lblConverterId.Size = new System.Drawing.Size(72, 15);
            this.lblConverterId.TabIndex = 4;
            this.lblConverterId.Text = "Converter Id";
            // 
            // lblInstrumentStatus
            // 
            this.lblInstrumentStatus.AutoSize = true;
            this.lblInstrumentStatus.Location = new System.Drawing.Point(346, 16);
            this.lblInstrumentStatus.Name = "lblInstrumentStatus";
            this.lblInstrumentStatus.Size = new System.Drawing.Size(100, 15);
            this.lblInstrumentStatus.TabIndex = 6;
            this.lblInstrumentStatus.Text = "Instrument Status";
            // 
            // cmbInstrumentStatus
            // 
            this.cmbInstrumentStatus.FormattingEnabled = true;
            this.cmbInstrumentStatus.Location = new System.Drawing.Point(452, 12);
            this.cmbInstrumentStatus.Name = "cmbInstrumentStatus";
            this.cmbInstrumentStatus.Size = new System.Drawing.Size(364, 23);
            this.cmbInstrumentStatus.TabIndex = 7;
            // 
            // btnGetInstruments
            // 
            this.btnGetInstruments.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetInstruments.Location = new System.Drawing.Point(884, 12);
            this.btnGetInstruments.Name = "btnGetInstruments";
            this.btnGetInstruments.Size = new System.Drawing.Size(130, 23);
            this.btnGetInstruments.TabIndex = 8;
            this.btnGetInstruments.Text = "Get Instruments";
            this.btnGetInstruments.UseVisualStyleBackColor = true;
            this.btnGetInstruments.Click += new System.EventHandler(this.btnGetInstruments_Click);
            // 
            // FormInstruments
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 590);
            this.Controls.Add(this.btnGetInstruments);
            this.Controls.Add(this.cmbInstrumentStatus);
            this.Controls.Add(this.lblInstrumentStatus);
            this.Controls.Add(this.lblConverterId);
            this.Controls.Add(this.txtConverterId);
            this.Controls.Add(this.btnGoToInstrument);
            this.Controls.Add(this.btnResponseInfos);
            this.Controls.Add(this.dgvInstruments);
            this.Name = "FormInstruments";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Instruments";
            ((System.ComponentModel.ISupportInitialize)(this.dgvInstruments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvInstruments;
        private System.Windows.Forms.Button btnResponseInfos;
        private System.Windows.Forms.Button lblGoToInstrument;
        private System.Windows.Forms.Button btnGoToInstrument;
        private System.Windows.Forms.TextBox txtConverterId;
        private System.Windows.Forms.Label lblConverterId;
        private System.Windows.Forms.Label lblInstrumentStatus;
        private System.Windows.Forms.ComboBox cmbInstrumentStatus;
        private System.Windows.Forms.Button btnGetInstruments;
    }
}