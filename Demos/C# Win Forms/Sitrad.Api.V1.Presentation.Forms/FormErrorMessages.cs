﻿using System;
using System.Windows.Forms;
using Sitrad.Api.Client.Models;

namespace Sitrad.Api.V1.Presentation.Forms
{
    public partial class FormErrorMessages : Form
    {
        public FormErrorMessages(Request requestInfo)
        {
            InitializeComponent();

            StartPosition = FormStartPosition.CenterParent;

            if(requestInfo == null )
                return;

            txtHttpStatusCode.Text = requestInfo.StatusCode.ToString();
            txtErrors.Text = string.Join(Environment.NewLine, requestInfo.Errors);
            txtBody.Text = JsonUtil.BeautifyJson(requestInfo.Body);
        }
    }
}
