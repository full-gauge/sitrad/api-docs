﻿
namespace Sitrad.Api.V1.Presentation.Forms
{
    partial class FormFunction
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.dgvValues = new System.Windows.Forms.DataGridView();
            this.txtName = new System.Windows.Forms.TextBox();
            this.lblName = new System.Windows.Forms.Label();
            this.lblStatus = new System.Windows.Forms.Label();
            this.lblId = new System.Windows.Forms.Label();
            this.txtId = new System.Windows.Forms.TextBox();
            this.btnGetAlarms = new System.Windows.Forms.Button();
            this.btnUpdateInstrument = new System.Windows.Forms.Button();
            this.cmbInstrumentStatus = new System.Windows.Forms.ComboBox();
            this.btnGetValues = new System.Windows.Forms.Button();
            this.btnGetFunctions = new System.Windows.Forms.Button();
            this.dgvFunctions = new System.Windows.Forms.DataGridView();
            this.lblValues = new System.Windows.Forms.Label();
            this.lblFuntions = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFunctions)).BeginInit();
            this.SuspendLayout();
            // 
            // dgvValues
            // 
            this.dgvValues.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvValues.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvValues.Location = new System.Drawing.Point(12, 147);
            this.dgvValues.Name = "dgvValues";
            this.dgvValues.RowTemplate.Height = 25;
            this.dgvValues.Size = new System.Drawing.Size(1002, 125);
            this.dgvValues.TabIndex = 0;
            // 
            // txtName
            // 
            this.txtName.Location = new System.Drawing.Point(57, 42);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(426, 23);
            this.txtName.TabIndex = 1;
            // 
            // lblName
            // 
            this.lblName.AutoSize = true;
            this.lblName.Location = new System.Drawing.Point(12, 45);
            this.lblName.Name = "lblName";
            this.lblName.Size = new System.Drawing.Size(39, 15);
            this.lblName.TabIndex = 3;
            this.lblName.Text = "Name";
            // 
            // lblStatus
            // 
            this.lblStatus.AutoSize = true;
            this.lblStatus.Location = new System.Drawing.Point(12, 82);
            this.lblStatus.Name = "lblStatus";
            this.lblStatus.Size = new System.Drawing.Size(39, 15);
            this.lblStatus.TabIndex = 4;
            this.lblStatus.Text = "Status";
            // 
            // lblId
            // 
            this.lblId.AutoSize = true;
            this.lblId.Location = new System.Drawing.Point(12, 9);
            this.lblId.Name = "lblId";
            this.lblId.Size = new System.Drawing.Size(17, 15);
            this.lblId.TabIndex = 6;
            this.lblId.Text = "Id";
            // 
            // txtId
            // 
            this.txtId.Enabled = false;
            this.txtId.Location = new System.Drawing.Point(57, 6);
            this.txtId.Name = "txtId";
            this.txtId.Size = new System.Drawing.Size(99, 23);
            this.txtId.TabIndex = 5;
            // 
            // btnGetAlarms
            // 
            this.btnGetAlarms.Location = new System.Drawing.Point(897, 12);
            this.btnGetAlarms.Name = "btnGetAlarms";
            this.btnGetAlarms.Size = new System.Drawing.Size(117, 23);
            this.btnGetAlarms.TabIndex = 7;
            this.btnGetAlarms.Text = "Get alarms";
            this.btnGetAlarms.UseVisualStyleBackColor = true;
            // 
            // btnUpdateInstrument
            // 
            this.btnUpdateInstrument.Location = new System.Drawing.Point(366, 9);
            this.btnUpdateInstrument.Name = "btnUpdateInstrument";
            this.btnUpdateInstrument.Size = new System.Drawing.Size(117, 23);
            this.btnUpdateInstrument.TabIndex = 8;
            this.btnUpdateInstrument.Text = "Update Instrument";
            this.btnUpdateInstrument.UseVisualStyleBackColor = true;
            // 
            // cmbInstrumentStatus
            // 
            this.cmbInstrumentStatus.FormattingEnabled = true;
            this.cmbInstrumentStatus.Location = new System.Drawing.Point(57, 79);
            this.cmbInstrumentStatus.Name = "cmbInstrumentStatus";
            this.cmbInstrumentStatus.Size = new System.Drawing.Size(426, 23);
            this.cmbInstrumentStatus.TabIndex = 9;
            // 
            // btnGetValues
            // 
            this.btnGetValues.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.btnGetValues.Location = new System.Drawing.Point(511, 9);
            this.btnGetValues.Name = "btnGetValues";
            this.btnGetValues.Size = new System.Drawing.Size(117, 23);
            this.btnGetValues.TabIndex = 10;
            this.btnGetValues.Text = "Get values";
            this.btnGetValues.UseVisualStyleBackColor = true;
            this.btnGetValues.Click += new System.EventHandler(this.btnGetValues_Click);
            // 
            // btnGetFunctions
            // 
            this.btnGetFunctions.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.btnGetFunctions.Location = new System.Drawing.Point(511, 41);
            this.btnGetFunctions.Name = "btnGetFunctions";
            this.btnGetFunctions.Size = new System.Drawing.Size(117, 23);
            this.btnGetFunctions.TabIndex = 12;
            this.btnGetFunctions.Text = "Get functions";
            this.btnGetFunctions.UseVisualStyleBackColor = true;
            // 
            // dgvFunctions
            // 
            this.dgvFunctions.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.dgvFunctions.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvFunctions.Location = new System.Drawing.Point(12, 307);
            this.dgvFunctions.Name = "dgvFunctions";
            this.dgvFunctions.RowTemplate.Height = 25;
            this.dgvFunctions.Size = new System.Drawing.Size(1002, 125);
            this.dgvFunctions.TabIndex = 11;
            // 
            // lblValues
            // 
            this.lblValues.AutoSize = true;
            this.lblValues.Location = new System.Drawing.Point(12, 129);
            this.lblValues.Name = "lblValues";
            this.lblValues.Size = new System.Drawing.Size(43, 15);
            this.lblValues.TabIndex = 13;
            this.lblValues.Text = "Values:";
            // 
            // lblFuntions
            // 
            this.lblFuntions.AutoSize = true;
            this.lblFuntions.Location = new System.Drawing.Point(12, 289);
            this.lblFuntions.Name = "lblFuntions";
            this.lblFuntions.Size = new System.Drawing.Size(59, 15);
            this.lblFuntions.TabIndex = 14;
            this.lblFuntions.Text = "Functions";
            // 
            // FormFunction
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1026, 555);
            this.Controls.Add(this.lblFuntions);
            this.Controls.Add(this.lblValues);
            this.Controls.Add(this.btnGetFunctions);
            this.Controls.Add(this.dgvFunctions);
            this.Controls.Add(this.btnGetValues);
            this.Controls.Add(this.cmbInstrumentStatus);
            this.Controls.Add(this.btnUpdateInstrument);
            this.Controls.Add(this.btnGetAlarms);
            this.Controls.Add(this.lblId);
            this.Controls.Add(this.txtId);
            this.Controls.Add(this.lblStatus);
            this.Controls.Add(this.lblName);
            this.Controls.Add(this.txtName);
            this.Controls.Add(this.dgvValues);
            this.Name = "FormFunction";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Instrument";
            ((System.ComponentModel.ISupportInitialize)(this.dgvValues)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dgvFunctions)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DataGridView dgvValues;
        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Label lblName;
        private System.Windows.Forms.Label lblStatus;
        private System.Windows.Forms.Label lblId;
        private System.Windows.Forms.TextBox txtId;
        private System.Windows.Forms.Button btnGetAlarms;
        private System.Windows.Forms.Button btnUpdateInstrument;
        private System.Windows.Forms.ComboBox cmbInstrumentStatus;
        private System.Windows.Forms.Button btnGetValues;
        private System.Windows.Forms.Button btnGetFunctions;
        private System.Windows.Forms.DataGridView dgvFunctions;
        private System.Windows.Forms.Label lblValues;
        private System.Windows.Forms.Label lblFuntions;
    }
}